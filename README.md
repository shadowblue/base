# shadowblue base image &nbsp; [![pipeline status](https://gitlab.com/shadowblue/base/badges/main/pipeline.svg)](https://gitlab.com/shadowblue/base/-/commits/main) &nbsp; [![Quay Images](https://img.shields.io/badge/Quay-Images-green)](https://quay.io/shadowblue/base)

A minimalistic image built on top of Fedora Silverblue with very few modifications. It's a good option for purists, as this image doesn't add anything special on top of Fedora Silverblue (no bloatware). If you're a newbie or not a Linux guru and you want something with more batteries included and better UX/defaults (still without any bloatware) you can look at the [main image](https://gitlab.com/shadowblue/main).

Changes in this image in comparison with Fedora Silverblue:

- Following RPM packages replaced with Flathub version:
  - Firefox
  - GNOME Terminal replaced with Ptyxis
  - GNOME System Monitor replaced with Mission Center
- All Fedora Flatpaks removed
- Following Flathub apps installed:
  - com.github.maoschanz.drawing
  - com.mattjakeman.ExtensionManager
  - io.bassi.Amberol
  - io.github.celluloid_player.Celluloid
  - org.gnome.baobab (Disk Usage Analyzer)
  - org.gnome.Calculator
  - org.gnome.Calendar
  - org.gnome.Characters
  - org.gnome.clocks
  - org.gnome.Contacts
  - org.gnome.Evince (Document Viewer)
  - org.gnome.FileRoller
  - org.gnome.font-viewer
  - org.gnome.Logs
  - org.gnome.Loupe (Image Viewer)
  - org.gnome.NautilusPreviewer
  - org.gnome.SimpleScan (Document Scanner)
  - org.gnome.Snapshot (Camera)
  - org.gnome.TextEditor
  - org.gnome.Weather
- Added [AllTheTools repo](https://gitlab.com/shadowblue/allthetools)
- Additional RPM packages:
  - distrobox
  - inxi
  - nvme-cli
  - smartmontools

P.S. You can see all reasons for this changes in [recipes directory](./recipes).

## Installation

> [!WARNING]
> [This is an experimental feature](https://www.fedoraproject.org/wiki/Changes/OstreeNativeContainerStable), try at your own discretion.

To rebase an existing atomic Fedora installation to the latest base image (see [available images](#available-images) and [available tags](#available-tags) for other options):

- **Recommended:** reset all modifications of immutable image (layered packages, overrides, etc):
  ```
  rpm-ostree reset
  ```
- Rebase to the unsigned image, to get the proper signing keys and policies installed:
  ```
  rpm-ostree rebase ostree-unverified-registry:quay.io/shadowblue/base:latest
  ```
- Reboot to complete the rebase:
  ```
  systemctl reboot
  ```
- Note that on the first boot after rebase, the system will remove all flatpak applications installed from the Fedora repo and install applications from Flathub. After installation, there is a chance that the icons of these applications will be missing. Don't panic, after the next rebase+reboot this should be fixed. If not, try switching the dark/light theme. If nothing helps, open the issue in this repo.
- Then rebase to the signed image, like so:
  ```
  rpm-ostree rebase ostree-image-signed:docker://quay.io/shadowblue/base:latest
  ```
- Reboot again to complete the installation
  ```
  systemctl reboot
  ```

## Schedule

- `base` images start building every day at 04:00 UTC. Usually they are ready at 04:10 UTC.
- `base-lts` images start building every day at 04:20 UTC. Usually they are ready at 04:30 UTC.
- `base-nvidia` images start building every day at 04:40 UTC. Usually they are ready at 04:50 UTC.

## Variants

### Supported versions

- Rawhide
- Branched on a best effort scenario. I can forget to enable it when branched starts in Fedora and enable it a few days or weeks later
- Latest Fedora release until official EoL
- Previous Fedora release until official EoL

### Available images

- `quay.io/shadowblue/base` — for all normal GPUs that don't require out-of-tree kernel modules (AMD, Intel)
- `quay.io/shadowblue/base-nvidia` — for Nvidia Maxwell and newer
- `quay.io/shadowblue/base-lts` — same as `base` but with kernel-longterm (lts)
- `quay.io/shadowblue/base-lts-nvidia` — same as `base-nvidia` but with kernel-longterm (lts)

**Warning**: lts and nvidia images require to disable Secure Boot!

What is kernel-longterm? It's an older kernel branch which doesn't receive big changes but it still receives security and bugfix updates for several years. You will miss newer features and new hardware support, but you will get (probably) more stable kernel. Usually we use latest LTS branch available.

Note: nvidia and lts images are built from their own branches in this repository.

### Available tags

- :rawhide (Not available for nvidia images)
- :latest (Tracks newest fedora version released. Version is replaced two weeks after the new release available.)
- :oldlatest (:latest version - 1)
- :${fedora_version} + 1 (e.g. :42. Available only if there is a branched version.)
- :${fedora_version} (e.g. :41)
- :${fedora_version} - 1 (e.g. :40)

Each tag has it's timestamp version for easier rollback and release pinning. E.g. :rawhide → :rawhide-20241030, :latest → :latest-20241030, etc.

### Tags expiration policy

All tags in registry for oldlatest version expire after 180 days. Tags for latest version expire after 365 days. Tags for rawhide and branched expire after 30 days.

## ISO

If build on Fedora Atomic, you can generate an offline ISO with the instructions available [here](https://blue-build.org/learn/universal-blue/#fresh-install-from-an-iso). These ISOs cannot unfortunately be distributed on GitLab for free due to large sizes, so for public projects something else has to be used for hosting.

## Verification

These images are signed with [Sigstore](https://www.sigstore.dev/)'s [cosign](https://github.com/sigstore/cosign). You can verify the signature by downloading the `cosign.pub` file from this repo and running the following command:

```bash
cosign verify --key cosign.pub quay.io/shadowblue/base:latest
```

## Contact us

- [Matrix Space](https://matrix.to/#/#shadowblue:matrix.org) (`#shadowblue:matrix.org`)
- [Telegram Chat](https://t.me/shadowblue_linux) (`@shadowblue_linux`)

## License

All license files can be found in the top directory of the source code or in /usr/share/licenses/shadowblue in the built images.

```
Copyright 2024 BlueBuild developers <bluebuild@xyny.anonaddy.com>
Copyright 2024-2025 Andrey Brusnik <dev@shdwchn.io>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
